<!DOCTYPE html>
<html lang="en">

<head>
    <title>MS Travels</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="assets/css/style.css">
    <script src="assets/js/script.js"></script>

    
</head>

<body>


    <div class="header">
        <div class="container d-flex">
            <div>
                <img src="assets/images/Logo.svg"> 
            </div>
            <ul class="d-flex">
                <li>About Us </li>
                <li>MSTT WOW </li>
                <li>City Guides</li>
                <li>Blog</li>
                <li>Contact Us</li>
            </ul>
            <div class="d-flex">
            
                <i data-feather="bell"></i>  
                <div class="user-details d-flex">
                    
                    <div>
                        <div class="dropdown">
                            <p class="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown">
                                <img class="user-img" src="assets/images/profile_img.png">
                              
                            </p>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                              <a class="dropdown-item" href="#">Action</a>
                              <a class="dropdown-item" href="#">Another action</a>
                              <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                          </div>
    
                    </div>
    
                </div>
            </div>

        </div>

    </div>

    <div class="container">
