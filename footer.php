<?php
include_once 'header.php';
?>

</div> <!-- container end -->



<div class="footer">
    <div class="container">
        <div class="about-block">
            <div class="sub-block">
                <div>
                    <label>About Us</label>
                    <p>
                        Our Story started back in 1997 with the inception of MS Tours & Travels Ltd. as a
                         Travel Management Company dealing with Hotel & Apartment reservations across the globe.
                         We have offices in London, UK and New Delhi, India.
                    </p>
                </div>
                <div class="mt-3">
                    <img class="mr-3" src="assets/images/footer2.png">
                    <img src="assets/images/footer1.png">                    
                </div>
            </div>
            <div class="sub-block">
                <label>Contact Us</label>
                <p>UK: +44 20 3328 9696 </p>
                <p>India: +91 11 41431347</p>
                <p>reservations@mstravels.com</p>
            </div>
            <div class="sub-block">
                <div>
                    <label>Subscribe Newsletter </label>
                    <p>
                        Get regular updates of fantastic offers we create for you.
                         Be it Spa, Golf, Boutique, Castles & Manors, 
                         Suite or Business Hotels - these offers will be hard to beat.
                    </p>
                </div>
                <div class="footer-search">
                    <input type="text" placeholder="Your Email Address">
                </div>
                <div class="mt-3">
                    <label>Follow us</label>
                    <div class="social-icons">

                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <p>© 2020 MS Tours and Travels Ltd. All Rights Reserved</p>
            <div>
                <p>
                    <a href="#">Terms & Conditions</a>  
                     |   
                    <a href="#">Privacy Policy</a>
                </p>
            </div>
    
        </div>

    </div>

</div>





<script src="assets/js/feather.min.js"></script>
<script>
    feather.replace();
</script>
</body>

</html>